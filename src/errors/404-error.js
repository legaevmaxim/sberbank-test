import BasicHttpError from '.';

export default class NotFoundError extends BasicHttpError {
  constructor(message) {
    super(message || 'Not Found', 404);
  }
}
