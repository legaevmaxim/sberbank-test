import BasicHttpError from './';

export default class BadGatewayError extends BasicHttpError {
  constructor(message = 'Bad Gateway') {
    super(message, 502);
  }
}
