import BasicHttpError from '.';

export default class UnauthorizedError extends BasicHttpError {
  constructor(message) {
    super(message || 'Unauthorized', 401);
  }
}
