import BasicHttpError from '.';

export default class AccessDeniedError extends BasicHttpError {
  constructor(message) {
    super(message || 'I’m a teapot', 418);
  }
}
