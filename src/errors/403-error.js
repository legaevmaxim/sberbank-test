import BasicHttpError from '.';

export default class ForbiddenError extends BasicHttpError {
  constructor(message) {
    super(message || 'Forbidden', 403);
  }
}
