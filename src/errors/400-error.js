import BasicHttpError from '.';

export default class BadRequestError extends BasicHttpError {
  constructor(message) {
    super(message || 'Bad Request', 400);
  }
}
