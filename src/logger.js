import { Stream } from 'stream';
import moment from 'moment';
import winston from 'winston';
import toUpper from 'lodash/toUpper';
import map from 'lodash/map';
import 'winston-daily-rotate-file';

const {
  LOG_LEVEL,
  LOG_CONSOLE_ALLOWED,
  npm_package_name,
  npm_package_version
} = process.env;

let instance;

function tryStringify(data) {
  try {
    if (data instanceof Stream) return 'Stream content';
    JSON.stringify(data);

    return data;
  } catch (error) {
    return data.toString();
  }
}

const format = winston.format.printf(
  ({ level, message, timestamp }) =>
    `${timestamp} ${level}: ${tryStringify(message)}`
);

export class Logger {
  static get() {
    if (!instance) Logger.update();

    return instance;
  }

  static update() {
    const commonSettings = {
        level: LOG_LEVEL || 'info',
        format: winston.format.combine(winston.format.timestamp(), format),
        timestamp: true
      },
      transports = [];

    if (LOG_CONSOLE_ALLOWED === 'true')
      transports.push(
        new winston.transports.Console({
          ...commonSettings
        })
      );

    transports.push(
      new winston.transports.DailyRotateFile({
        ...commonSettings,
        filename: `${npm_package_name}-%DATE%.log`,
        dirname: 'logs',
        maxFiles: '30d'
      })
    );

    instance = new winston.createLogger({ transports });

    const { info, error, debug, warn } = instance;

    instance.info = ((...args) => info(Logger.stringify(...args)));
    instance.error = ((...args) => error(Logger.stringify(...args)));
    instance.debug = ((...args) => debug(Logger.stringify(...args)));
    instance.warn = ((...args) => warn(Logger.stringify(...args)));
  }

  static stringify(...args) {
    const result = map(args, arg => JSON.stringify(tryStringify(arg)));
    return result.join(',');
  }
}

export default function log() {
  return Logger.get();
}

export async function logRequestResult(
  req,
  res,
  level = 'info',
  logResponseBody = true
) {
  try {
    req.endProcessing = moment();
    log().log({
      level,
      message: JSON.stringify({
        npm_package_name,
        npm_package_version,
        request: {
          method: toUpper(req.method),
          path: req.path,
          url: req.url,
          headers: req.headers,
          session: req.session,
          body: tryStringify(req.body),
          params: tryStringify(req.params),
          duration: req.endProcessing.diff(req.startProcessing)
        },
        response: {
          status: res.status,
          body: logResponseBody
            ? tryStringify(res.data)
            : 'Response body logging is disabled for this request',
          stack: res.stack,
          code: res.code,
          message: res.message
        }
      })
    });
  } catch (ex) {
    log().error('Failed to log request result', req.url, ex);
  }
}

process.on('unhandledRejection', async (reason, promise) =>
  log().error(('Unhandled rejection at: ', promise, ' reason: ', reason))
);
process.on('warning', async warn => log().warn(warn.stack));
