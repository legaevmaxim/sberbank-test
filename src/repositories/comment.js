import Db from '../db/models';
import BadRequestError from '../errors/400-error';

const Comment = Db.sequelize.models.Comment;

export default class CommentRepository {
  static async create(data) {
    return Comment.create(data);
  }

  static async findAll(BlogId) {
    return Comment.findAll({where: { BlogId}});
  }

  static async findOne(BlogId, id) {
    return Comment.findOne({where: { BlogId, id }});
  }

  static async delete(BlogId, id) {
    const result = await Comment.destroy({where: { BlogId, id }});
    if (!result)
      throw new BadRequestError(`Comment with BlogId:${BlogId} and id:${id} not found`);
    else return {deletedRows: result};
  }

  static async update(data, BlogId, id) {
    return Comment.update(data, {where: { BlogId, id }});
  }

}
