import Db from '../db/models';
import BadRequestError from '../errors/400-error';

const Blog = Db.sequelize.models.Blog;

export default class BlogRepository {
  static async create(data) {
    return Blog.create(data);
  }

  static async findAll() {
    return Blog.findAll();
  }

  static async findOne(id) {
    return Blog.findOne({where: { id }});
  }

  static async delete(id) {
    const result = await Blog.destroy({where: { id }});
    if (!result)
      throw new BadRequestError(`Блога с id:${id} не существует`);
    else return {deletedRows: result};
  }

  static async update(data, id) {
    return Blog.update(data, {where: { id }});
  }
}
