module.exports = (sequelize, DataTypes) => {
  const Blog = sequelize.define('Blog', {
    name: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false
    },
    description: {
      type: DataTypes.STRING,
      unique: false,
      allowNull: false
    }
  });

  Blog.associate = models => {
    models.Blog.hasMany(models.Comment, { onDelete: 'cascade' });
  };

  return Blog;
};