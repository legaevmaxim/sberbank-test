import Joi from 'joi';

export async function validatePostRequestPayload({
  comment,
  BlogId
} = {}) {
  const schema = Joi.object({
    comment: Joi.string()
        .min(1)
        .required(),
    BlogId: Joi.number()
        .min(1)
        .required()
  });

  return schema.validate({ comment, BlogId });
}

export async function validatePutRequestPayload({
  comment
} = {}) {
  const schema = Joi.object({
    comment: Joi.string()
        .min(1)
        .required()
  });

  return schema.validate({ comment });
}
