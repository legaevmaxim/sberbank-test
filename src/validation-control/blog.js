import Joi from 'joi';

export async function validatePostRequestPayload({
  name,
  description
} = {}) {
  const schema = Joi.object({
    name: Joi.string()
        .min(1)
        .max(30)
        .required(),
    description: Joi.string()
        .min(5)
  });

  return schema.validate({ name, description });
}

export async function validatePutRequestPayload({
  name,
  description
} = {}) {
  const schema = Joi.object({
    name: Joi.string()
        .min(1)
        .max(30),
    description: Joi.string()
        .min(5)
  });

  return schema.validate({ name, description });
}
