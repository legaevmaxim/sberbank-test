import moment from 'moment/moment';
import bodyParser from 'body-parser';
import auth from 'basic-auth';
import { checkAuth } from '../utils';
import { AUTH_URI_LIST } from '../const';

export default app => {
  app.use((req, res, next) => {
    req.startProcessing = moment();
    next();
  });

  app.use((req, res, next) => {
    req.startProcessing = moment();
    next();
  });

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));

  app.post(AUTH_URI_LIST.POST, (req, res, next) => {
    const credentials = auth(req);
    if (!credentials || !checkAuth(credentials.name, credentials.pass)) {
      res.statusCode = 401;
      res.setHeader('WWW-Authenticate', 'Basic realm="example"');
      res.end('Access denied');
    }
    next();
  });

  app.put(AUTH_URI_LIST.PUT, (req, res, next) => {
    const credentials = auth(req);
    if (!credentials || !checkAuth(credentials.name, credentials.pass)) {
      res.statusCode = 401;
      res.setHeader('WWW-Authenticate', 'Basic realm="example"');
      res.end('Access denied');
    }
    next();
  });

  app.delete(AUTH_URI_LIST.DELETE, (req, res, next) => {
    const credentials = auth(req);
    if (!credentials || !checkAuth(credentials.name, credentials.pass)) {
      res.statusCode = 401;
      res.setHeader('WWW-Authenticate', 'Basic realm="example"');
      res.end('Access denied');
    }
    next();
  });
};