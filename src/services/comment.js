import { validatePostRequestPayload, validatePutRequestPayload } from '../validation-control/comment';
import CommentRepository from '../repositories/comment';
import BlogRepository from '../repositories/blog';
import NotFoundError from '../errors/404-error';

export default class CommentService {
  static async create(data, blogId) {
    data = {...data, BlogId: blogId};
    const blog = await BlogRepository.findOne(blogId);
    if (!blog)
      throw new NotFoundError(`Blog with id:${blogId} not found`);
    await validatePostRequestPayload(data);

    return CommentRepository.create(data);
  }

  static async update(data, blogId, id) {
    const comment = await CommentRepository.findOne(blogId, id);
    if (!comment)
      throw new NotFoundError(`comment with id:${id} and blogId:${blogId} not found`);
    await validatePutRequestPayload(data);

    return CommentRepository.update(data, blogId, id);
  }

  static async findAll(blogId) {
    return CommentRepository.findAll(blogId);
  }

  static async findOne(blogId, id) {
    return CommentRepository.findOne(blogId, id);
  }

  static async delete(blogId, commentId) {
    return CommentRepository.delete(blogId, commentId);
  }

}