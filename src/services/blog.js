import { validatePostRequestPayload, validatePutRequestPayload  } from '../validation-control/blog';
import BlogRepository from '../repositories/blog';
import NotFoundError from '../errors/404-error';


export default class BlogService {
  static async create(data) {
    await validatePostRequestPayload(data);

    return BlogRepository.create(data);
  }

  static async update(data, blogId) {
    const blog = await BlogRepository.findOne(blogId);
    if (!blog)
      throw new NotFoundError(`Blog with id:${blogId} not found`);
    await validatePutRequestPayload(data);

    return BlogRepository.update(data, blogId);
  }

  static async findAll(data) {
    return BlogRepository.findAll(data);
  }

  static async findOne({ id }) {
    return BlogRepository.findOne(id);
  }

  static async delete({ id }) {
    return BlogRepository.delete(id);
  }

}