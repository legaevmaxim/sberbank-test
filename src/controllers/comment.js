import CommentService from '../services/comment';

export default class JustAskController {
  static async findAll({params}) {
    return CommentService.findAll(params.blogId);
  }

  static async findOne({params}) {
    return CommentService.findOne(params.blogId, params.id);
  }

  static async create({body, params}) {
    return CommentService.create(body, params.blogId);
  }

  static async update({body, params}) {
    return CommentService.update(body, params.blogId, params.id);
  }

  static async delete({params}) {
    return CommentService.delete(params.blogId, params.id);
  }
}
