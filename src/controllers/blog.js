import BlogService from '../services/blog';


export default class JustAskController {
  static async findAll(req) {
    return BlogService.findAll(req);
  }

  static async findOne({params}) {
    return BlogService.findOne(params);
  }

  static async create({body}) {
    return BlogService.create(body);
  }

  static async update({body, params}) {
    return BlogService.update(body, params.id);
  }

  static async delete({params}) {
    return BlogService.delete(params);
  }
}
