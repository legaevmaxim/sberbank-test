import BlogController from '../../controllers/blog';
import handle from '../..//request-handler';
import { addApiVersion } from '../../utils';


export default (app, version) => {
  app.get(addApiVersion('/blogs', version), handle(BlogController.findAll));
  app.get(addApiVersion('/blogs/:id', version), handle(BlogController.findOne));
  app.post(addApiVersion('/blogs', version), handle(BlogController.create));
  app.put(addApiVersion('/blogs/:id', version), handle(BlogController.update));
  app.delete(addApiVersion('/blogs/:id', version), handle(BlogController.delete));
};
