import CommentController from '../../controllers/comment';
import handle from '../../request-handler';
import { addApiVersion } from '../../utils';


export default (app, version) => {
  app.get(addApiVersion('/blogs/:blogId/comments', version), handle(CommentController.findAll));
  app.get(addApiVersion('/blogs/:blogId/comments/:id', version), handle(CommentController.findOne));
  app.post(addApiVersion('/blogs/:blogId/comments', version), handle(CommentController.create));
  app.put(addApiVersion('/blogs/:blogId/comments/:id', version), handle(CommentController.update));
  app.delete(addApiVersion('/blogs/:blogId/comments/:id', version), handle(CommentController.delete));
};
