
import blog from './blog';
import comment from './blog/comment';

import { CURRENT_API_VERSION } from '../const';

export default app => [blog, comment].forEach(route => route(app, CURRENT_API_VERSION));
