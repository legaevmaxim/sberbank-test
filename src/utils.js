import compare from 'tsscmp';

export function addApiVersion(url, version) {
  return version ? `/api/v${version}${url}` : url;
}

export function checkAuth(name, pass) {
  let valid = true;

  valid = compare(name, 'max') && valid;
  valid = compare(pass, 'test') && valid;

  return valid;
}