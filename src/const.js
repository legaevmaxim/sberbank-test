export const CURRENT_API_VERSION = '1';

export const BASE_PATH = `/api/v${CURRENT_API_VERSION}`,
  BLOG_BASE_PATH = `${BASE_PATH}/blogs`,
  COMMENT_BASE_PATH = `${BLOG_BASE_PATH}/:blogId/comments`,
  AUTH_URI_LIST = {
    POST: [
      BLOG_BASE_PATH,
      COMMENT_BASE_PATH
    ],
    PUT: [
      `${BLOG_BASE_PATH}/:id`,
      `${COMMENT_BASE_PATH}/:id`
    ],
    DELETE: [
      `${BLOG_BASE_PATH}/:id`,
      `${COMMENT_BASE_PATH}/:id`
    ]
  };