import { Stream } from 'stream';
import log, { logRequestResult } from './logger';

export default function handle(handler, logResponseBody = true) {
  return async (req, res) => {
    handler(req, res)
      .then(result => {
        if (result && result.contentType) {
          res.set('Content-Type', result.contentType);
          if (result.contentDisposition) res.set('Content-Disposition', result.contentDisposition);
          if (result.data instanceof Stream) {
            result.data.on('error', err => log().error(err));

            result.data.pipe(res);
          } else res.status(200).send(result.data);
        } else res.status(200).send(result);

        logRequestResult(
          req,
          {
            status: 200,
            data: result && result.contentType ? result.data : result
          },
          'info',
          logResponseBody
        );
      })
      .catch(ex => {
        const status = ex.status || 500;
        logRequestResult(
          req,
          {
            status,
            body: ex.message,
            stack: ex.stack
          },
          'error'
        );
        res.status(status).send(ex.message);
      });
  };
}
