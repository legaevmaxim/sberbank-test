import 'dotenv/config';
import http from 'http';
import express from 'express';
import moment from 'moment/moment';

import routes from './routes';
import log from './logger';
import Db from './db/models';

const { PORT } = process.env;

moment.locale('ru');
const app = express(),
  server = http.createServer(app);

const middleware = require('./middleware').default;

middleware(app);

routes(app);

Db.sequelize.sync().then(() => {
  server.listen(PORT, () => log().info(`Service started on port ${PORT}`));
});
